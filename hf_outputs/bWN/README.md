This directory contains the hisfitter outputs in .xml format for the bffN_btag signal region
fit and signal region provided by Tomohiro Yamazaki <tomohiro.yamazaki@cern.ch> and Zulit Paola Arrubarrena Tame <paola.arrubarrena@cern.ch>

