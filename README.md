# 1) Instructions on how to produce the workspaces for exclusions in .xml format.
1. Convert the HistFitter output file to xml file:

    the output file of interest is the one with "combined_NormalMeasurement.root" in the name.

    open it with root, and call 
>'NormalMeasurement->PrintXML()' 
this will create an xml file in the current directory you are in.

2. Convert the xml file to json file:

    just call 
>'pyhf xml2json config/3b_tag21.2.27-1_RW_ExpSyst_36100_multibin_bkg/Excl.xml'

    If you dont have pyhf yet, simply install it with 
>'pip install pyhf --user'

That's it! In the end there should be a json file for each signal point which contains all the yields and uncertainties. And we need this for every separate fits we did: 2-body, 2-body-diagonal, 3-body, and 4-body.

And no, you don't need to rerun HistFitter, just start from the HF output file.

Official instruction can be found here: >https://pyhf.readthedocs.io/en/v0.6.0/babel.html

Please contact @sandrean if anything's unclear!



# 2) Running PyHF


You can also find instructions corresponding to steps b) to i) here:
> https://gitlab.cern.ch/sandrean/pyhf_conversions/-/tree/stop1L_WIP/

a) Complete the HistFactory tutorial
> https://pyhf.github.io/tutorial-PyHEP-2020/introduction.html

b) Download HF files into /Users/cclement/Desktop/pMSSM/pyhf_conversions from 
> https://gitlab.cern.ch/sandrean/pyhf_conversions/-/tree/stop1L_WIP/analyses/stop1L/hf_outputs

> git clone --single-branch --branch stop1L_WIP  https://cclement@gitlab.cern.ch/sandrean/pyhf_conversions.git

(root files where not on git, had to scp from lxplus)

c) >xml2json< see doc at >https://scikit-hep.org/pyhf/cli.html#pyhf-xml2json

Makes .json for a single model point (need to do this step for every signal):

>pyhf xml2json --track-progress results/tN_med_excl_85_v2.11_bWN_400_235/NormalMeasurement.xml

d) Create signal_and_bkg.json with a single signal and all backgrounds:

>In >/Users/cclement/Desktop/pMSSM/pyhf_conversions/analyses/stop1L/hf_outputs<:
pyhf xml2json --track-progress results/tN_med_excl_85_v2.11_bWN_400_235/NormalMeasurement.xml --output-dir myoutputs/allinfo_bWN_400_235.json

Then generate similar files allinfo_<channelname>_<mstop>_<mchi>.json

Then create a single BkgOnly.json by taking one of allinfo_<channelname>_<mstop>_<mchi>.json  files and stripping it of the the signal information, editing the file by hand.

Script I made a script to generate all the files allinfo_<channelname>_<mstop>_<mchi>.json:

>/Users/cclement/Desktop/pMSSM/pyhf_conversions/generate_signal.py

the outputs are stored in myoutputs


e) Generate signal patch (for what a signal patch is see HistFactory tutorial in a) )

In directory: >/Users/cclement/Desktop/pMSSM/pyhf_conversions


Make signal patch with a single model point:

>python3 make_signalpatch.py --bkg-only=analyses/stop1L/hf_outputs/BkgOnly.json --pattern="{mstop:d}_{mchi:d}" --hepdata ins123 analyses/stop1L/hf_outputs/allinfo_bWN_400_235.json --output-file signalpatch.json


Make signal patch for all models:

>python3 make_signalpatch.py --bkg-only=analyses/stop1L/hf_outputs/BkgOnly.json --pattern="{mstop:d}_{mchi:d}" --hepdata ins123 myoutputs/*json --output-file  allignalpatch.json



"{mstop:d}_{mchi:d}" -> will ensure that the mass variables are called mstop and mchi in the .json file and the ":d" ensures that parse.search looks for numbers during the parsing phase.

Then move the results:
>BkgOnly.json 
and 
>allignalpatch.json 
to:
>/Users/cclement/Desktop/pMSSM/pyhf_conversions/analyses/stop1L/likelihoods/
so it can be used in the next step f) 


f) Run HistFitter over the full likelihood and the entire patchset:
python3 run_patchset.py --group stop1L --patchset allignalpatch.json
(use --simplified to run over the simplified likelihood. The file patch name given after --patchset  is only needed if one does not use the default patch file name.) 

run_patchset.py uses the following inputs:
>/Users/cclement/Desktop/pMSSM/pyhf_conversions/analyses/stop1L/likelihoods/
BkgOnly.py
	allignalpatch.json

Produces one output file per model, they are stored here:
>/Users/cclement/Desktop/pMSSM/pyhf_conversions/analyses/stop1L/results

And produces one file per signal model, containing the following outputs: -2sigma, -1sigma, Expected, +1 sigma, +2 sigma, CLs_obs
>{'CLs_exp': [0.9818612698118051, 0.9882983434453159, 0.9938545955316929, 0.9977763172628745, 0.9995711798428342], 'CLs_obs': 0.9938921308580099}


g) Collect the fit resutls into a single .json that will be used to make plots.

> python3 harvest.py --no-simplified --group stop1L

Uses as input:	>   analyses/stop1L/results
Produces output:>   analyses/stop1L/harvests/harvest_stop1L.json

The output of harvest.py automatically calls the masses m1, m2

h) Create TGraphs from .json

>python harvestToContours.py --inputFile analyses/stop1L/harvests/harvest_stop1L.json --outputFile testgraph.root --interpolationScheme rbf --interpolation linear --xVariable m1 --yVariable m2 --xMin 0 --xMax 1500 --yMin 0 --yMax 1000 --level 1.64485362695 --smoothing 1 ;mv testgraph.root analyses/stop1L/graphs/

Then move testgraph.root to analyses/stop1L/graphs/
m1 and m2 need to be the name of the masses and match the content of the file harvest_stop1L.json



i) Draw the final contour plots (also define colors and all kinds on the plot style):
>python plot_contours.py --group stop1L  --comEnergy 13 --xmin 0 --xmax 1500 --ymin 0 --ymax 1000 --draw-CLs -l TEXT  -c CONTOUR

Uses as input:
>analyses/stop1L/graphs/testgraph.root (testgraph.root is output of step h)

# 3) Produce output which is the exclusion contour:

> analyses/stop1L/plots/exclusion_stop1L.pdf

Production of the simplified likelihood
a) Install simplified likelhood
Install >simplify-hep
See doc at: >https://gitlab.cern.ch/eschanet/simplify
>python3 -m pip install simplify-hep[contrib]

b) Run the simplification for the background only model: BkgOnly.json
> simplify convert  /Users/cclement/Desktop/pMSSM/pyhf_conversions/analyses/stop1L/likelihoods/BkgOnly.json > simplified_BkgOnly.json

Input: background only file analyses/stop1L/likelihoods/BkgOnly.json
Output: >simplified_BkgOnly.json<  move it to the output directory: analyses/stop1L/likelihoods/

Important notes, to make this work:
Need to edit the 'poi' in input BkgOnly.json from 'mu_sig' to 'lumi'
(reason why is explained here: https://github.com/scikit-hep/pyhf/issues/514 )
then you have to change it back when you do the fit.
Also there seems to be a problem with simplify convert, namely that in the output simplified_BkgOnly.json, the measurement section is mostly missing. This section needs to be readded by hand properly after the simplification step, before it can be used in HistFitter. 

c) Run the simplification for the signal: make a simplified signal patch
> python3 make_simplified_signalpatch.py --group stop1L --output-file simplified_signal_patch.json; mv simplified_signal_patch.json analyses/stop1L/likelihoods

Requires input: patchset.json (I use a symbolic link patchsetjson -> allignalpatch.json, located in analysis/stop1L/likelihoods) 
Output:  simplified_signal_patch.json
Move output simplified_signal_patch.json to analyses/stop1L/likelihoods/

d) Run HistFitter on the simplified likelihood
> python3 run_patchset.py --group stop1L --simplified --likelihood BkgOnly.json --patchset signal_patch.json

Inputs are taken from the directory:
analyses/stop1L/likelihoods/
following the command above (d) the input files are: "simplified_signal_patch.json" and simplified_BkgOnly.json

Outputs are produced in: analyses/stop1L/results/ and are of the form: simplified_stop1L_allinfo_tN_950_500.json (one file per model). 

e) Collect the fit resutls into a single .json that will be used to make plots.

>python3 harvest.py --simplified --group stop1L

Uses as input:	   		analyses/stop1L/results
Produces output:			analyses/stop1L/harvests/harvest_simplified_stop1L.json

The output of harvest.py automatically calls the masses m1, m2

f) Create TGraphs from .json

>python harvestToContours.py --inputFile analyses/stop1L/harvests/harvest_simplified_stop1L.json --outputFile simplified_testgraph.root --interpolationScheme rbf --interpolation linear --xVariable m1 --yVariable m2 --xMin 0 --xMax 1500 --yMin 0 --yMax 1000 --level 1.64485362695 --smoothing 1 ;mv testgraph.root analyses/stop1L/graphs/

Then move testgraph.root to analyses/stop1L/graphs/
m1 and m2 need to be the name of the masses and match the content of the file harvest_stop1L.json

g) Draw the final contour plots (also define colors and all kinds on the plot style):
>python plot_contours.py --group stop1L  --comEnergy 13 --xmin 0 --xmax 1500 --ymin 0 --ymax 1000 --draw-CLs -l TEXT  -c CONTOUR

Uses as input:
>analyses/stop1L/graphs/testgraph.root (testgraph.root is output of step h)

Produces output which is the exclusion contour:
>analyses/stop1L/plots/exclusion_stop1L.pdf

# 4) TODO List
- More automated
- Push all the scripts we made so far
- ...